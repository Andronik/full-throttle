#!/usr/bin/env python
# coding=UTF-8
 
#==============================================================================
# titre           :Pleine_Vitesse.py
# description     :Jeu où il faut éviter les autres autos
# author          :Andronikos Karkaselis
# date            :20190516
# version         :4.2
# usage           :python Pleine_Vitesse.py
# notes           :
# python_version  :3.7.1
#==============================================================================

from os import environ
import pygame as pg
import time
import random

environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (300, 30)
pg.init()
pg.display.set_mode((100, 100))
pg.display.set_caption("À Pleine Vitesse")
pg.mouse.set_visible(0)
largeur_window = 800
hauteur_window = 700
gameWindow = pg.display.set_mode((largeur_window, hauteur_window))
noir = (0, 0, 0)
blanc = (255, 255, 255)
rouge = (255, 0, 0)
clock = pg.time.Clock()
menu_img = pg.image.load("Images/menu_img2.jpg")
crash_img = pg.image.load("Images/crash.png")
crash_big = pg.image.load("Images/crash_image.png")
car_img = pg.image.load("Images/car_player.png")
liste_auto = [pg.image.load("Images/car1.png"), pg.image.load("Images/car2.png"),
            pg.image.load("Images/car3.png"), pg.image.load("Images/car4.png"),
            pg.image.load("Images/car5.png"), pg.image.load("Images/car6.png"),
            pg.image.load("Images/car7.png"), pg.image.load("Images/car8.png")]
road_img = pg.image.load("Images/road.png")

#=============================== classes des autos ================================================#

class player(object):
    def __init__(self, pos_x, pos_y):
        '''les variables de l'auto du joueur'''
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.largeur_auto = 44
        self.hauteur_auto = 90
        self.vitesse_x = 5
        self.vitesse_y = 0.75
        self.boite_car = (self.pos_x, self.pos_y, self.largeur_auto, self.hauteur_auto)

    def afficher(self):
        '''afficher l'auto du joueur'''
        boite_car = pg.Rect(self.pos_x + 4, self.pos_y + 3, self.largeur_auto, self.hauteur_auto)
        ##pg.draw.rect(gameWindow, blanc, boite_car, 1)
        gameWindow.blit(car_img, (self.pos_x, self.pos_y))
        return boite_car

class auto_trafic(object):
    def __init__(self):
        '''les variables des autres autos'''
        self.voie_x = 0
        self.pos_y = -100
        self.largeur_auto = 42
        self.hauteur_auto = 94
        self.vitesse_y = self.vitesse_random()
        self.auto_img = self.auto_random()
        self.boite_car = (self.voie_x, self.pos_y, self.largeur_auto, self.hauteur_auto)

    def auto_random(self):
        '''choisi au hasard une image pour les autres autos'''
        return random.choice(liste_auto)

    def vitesse_random(self):
        '''choisi au hasard une vitesse pour les autres autos'''
        liste_vitesse = [2.5, 3, 3.5, 4, 4.5]
        return random.choice(liste_vitesse)

    def afficher(self, player_boite, voie_x, vitesse_y, auto_img, points):
        '''afficher les autres autos selon leur voie, leur vitesse et leur image'''
        if self.pos_y >= hauteur_window:
            self.pos_y = -100
            self.auto_img = self.auto_random()
            self.vitesse_y = self.vitesse_random()
        else:
            self.pos_y += self.vitesse_y
        if voie_x >= 350: #pour les autos dans les voies de droite
            boite_trafic = pg.Rect(voie_x + 3, self.pos_y + 3, self.largeur_auto, self.hauteur_auto)
            ##pg.draw.rect(gameWindow, noir, boite_trafic, 1)
            gameWindow.blit(self.auto_img, (voie_x, self.pos_y))
        else: #pour les autos dans les deux voies de gauche
            boite_trafic = pg.Rect(voie_x + 3, self.pos_y*8, self.largeur_auto, self.hauteur_auto)
            pg.draw.rect(gameWindow, noir, boite_trafic, 1)
            auto_img = pg.transform.flip(self.auto_img, False, True)
            gameWindow.blit(auto_img, (voie_x, self.pos_y*8))
        collision_detect(player_boite, boite_trafic, points)

#=========================== divers fonctions =====================================================#

def afficher_route(rel_vit):
    '''affiche la route (divisée en quatre pour une meilleure performance)'''
    gameWindow.fill(noir)
    gameWindow.blit(road_img, (230, rel_vit - hauteur_window))
    gameWindow.blit(road_img, (230, rel_vit-525))
    gameWindow.blit(road_img, (230, rel_vit-350))
    gameWindow.blit(road_img, (230, rel_vit-175))
    gameWindow.blit(road_img, (230, rel_vit))
    gameWindow.blit(road_img, (230, rel_vit+175))
    gameWindow.blit(road_img, (230, rel_vit+350))
    gameWindow.blit(road_img, (230, rel_vit+525))

def collision_detect(boite_car, boite_trafic, points):
    '''détecte s'il y a une collision entre les autos selon leurs boites (hitboxes)'''
    if boite_trafic.colliderect(boite_car):
        gameWindow.blit(crash_img, boite_car)
        crash(points)

def compteur(points, pointage):
    '''affiche les points du joueur et le meilleur score'''
    font = pg.font.SysFont(None, 30)
    points_haut = font.render("Highscore: " + str(pointage), True, blanc)
    points_joueur = font.render("Score: " + str(points), True, blanc)
    gameWindow.blit(points_haut, (1, 1))
    gameWindow.blit(points_joueur, (1, 20))

def crash(points):
    '''affiche le nombres de points si il y a une collision et retourne au menu'''
    pg.mixer.music.stop()
    pg.mixer.music.load("Son/explosion.mp3")
    pg.mixer.music.play(0)
    font = pg.font.SysFont(None, 80)
    texte = font.render("Points: "+ str(points), True, noir)
    gameWindow.blit(crash_big, (0, 0))
    gameWindow.blit(texte, (235, 352))
    pg.display.update()
    sauvegarder_pointage(points)
    time.sleep(3)
    menu()

def haut_score():
    '''prend d'un fichier le plus haut score s'il y en a un'''
    try:
        with open("Fichiers/Pointage.txt", "r") as fichier:
            pointage = int(fichier.readline())
    except:
        pointage = 0
    return pointage

def parametre_sauvegarde():
    '''vérifie les paramètres sauvegardés à propos de la musique'''
    try:
        with open("Fichiers/Parametres.txt", "r") as fichier:
            musique = fichier.readline()
    except:
        musique = "NON"
    return musique

def sauvegarder_option_musique(choix):
    '''change dans le fichier de paramètres l'option de la musique'''
    with open("Fichiers/Parametres.txt", "w") as fichier:
            fichier.write(choix)

def sauvegarder_pointage(points):
    '''sauvegarde le pointage si il est plus haut que le précédant'''
    pointage = haut_score()
    if pointage < points:
        with open("Fichiers/Pointage.txt", "w") as fichier:
            fichier.write(str(points))

def parametres():
    '''menu des paramètres'''
    font_titre = pg.font.SysFont(None, 80)
    font_item = pg.font.SysFont(None, 45)
    pos_selection = 245
    choix = parametre_sauvegarde()
    menu_parametres = True
    while menu_parametres == True:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                pg.quit()
                quit()
            if pg.key.get_pressed()[pg.K_ESCAPE]:
                menu_parametres = False
            elif pg.key.get_pressed()[pg.K_UP] and pos_selection > 245:
                pos_selection -= 80
            elif pg.key.get_pressed()[pg.K_DOWN] and pos_selection < 405:
                pos_selection += 80
            elif pg.key.get_pressed()[pg.K_RIGHT] or pg.key.get_pressed()[pg.K_RETURN]:
                if pos_selection == 245:
                    if choix == "OUI":
                        choix = "NON"
                    elif choix == "NON":
                        choix = "OUI"
                    sauvegarder_option_musique(choix)
                    la_musique()
                #elif pos_selection == 325:
                elif pos_selection == 405:
                    menu_parametres = False
        gameWindow.fill(noir)
        item_titre = font_titre.render("PARAMÈTRES", True, rouge, noir)
        item_musique = font_item.render("MUSIQUE: ", True , blanc, noir)
        choix_musique = font_item.render(choix, True, rouge, noir)
        item_difficulte = font_item.render("DIFFICULTÉ:", True, (150, 150, 150), noir)
        item_retourner = font_item.render("RETOURNER", True, blanc, noir)
        selection = font_item.render(" > ", True, rouge, noir)
        gameWindow.blit(item_titre, (190, 90))
        gameWindow.blit(item_musique, (240, 250))
        gameWindow.blit(choix_musique, (415, 250))
        gameWindow.blit(item_difficulte, (240, 330))
        gameWindow.blit(item_retourner, (240, 410))
        gameWindow.blit(selection, (180, pos_selection))
        pg.display.update()
        clock.tick(30)

def la_musique():
    '''joue la musique si son option sauvegardé est "OUI"'''
    if parametre_sauvegarde() == "OUI":
        pg.mixer.music.load("Son/arcade-music-loop.wav")            
        pg.mixer.music.play(-1)
    else:
        pg.mixer.music.stop()

def menu():
    '''menu principal'''
    la_musique()
    font_titre = pg.font.SysFont(None, 80)
    font_item = pg.font.SysFont(None, 60)
    pos_selection = 245
    le_menu = True
    while le_menu == True:
        for event in pg.event.get():
            if event.type == pg.QUIT or pg.key.get_pressed()[pg.K_ESCAPE]:
                pg.quit()
                quit()
            elif pg.key.get_pressed()[pg.K_UP] and pos_selection > 245:
                pos_selection -= 80
            elif pg.key.get_pressed()[pg.K_DOWN] and pos_selection < 405:
                pos_selection += 80
            elif pg.key.get_pressed()[pg.K_RETURN]:
                if pos_selection == 245:
                    boucle_jeu()
                elif pos_selection == 325:
                    parametres()
                elif pos_selection == 405:
                    pg.quit()
                    quit()
        gameWindow.blit(menu_img, (0, 0))
        item_titre = font_titre.render("PLEINE VITESSE!", True, rouge, noir)
        item_commencer = font_item.render("COMMENCER", True , blanc, noir)
        item_parametres = font_item.render("PARAMÈTRES", True, blanc, noir)
        item_quitter = font_item.render("QUITTER", True, blanc, noir)
        selection = font_item.render(" > ", True, rouge, noir)
        gameWindow.blit(item_titre, (175, 90))
        gameWindow.blit(item_commencer, (260, 250))
        gameWindow.blit(item_parametres, (260, 330))
        gameWindow.blit(item_quitter, (260, 410))
        gameWindow.blit(selection, (180, pos_selection))
        pg.display.update()
        clock.tick(30)

#========================= boucle de jeu ==========================================================#

def boucle_jeu():
    '''boucle de jeu'''
    vitesse_route = 20
    rel_vit = 0
    acceleration = 0
    pleine_vitesse = False
    quitter = False
    pointage = haut_score()
    points = 0
    car_trafic1 = auto_trafic()
    car_trafic2 = auto_trafic()
    car_trafic3 = auto_trafic()
    car_trafic4 = auto_trafic()
    car = player(414, 560)
    while not quitter:
        keys = pg.key.get_pressed()
        for event in pg.event.get():
            if keys[pg.K_ESCAPE]:
                quitter = True
            elif event.type == pg.QUIT:
                pg.quit()
                quit()
        if keys[pg.K_LEFT] and car.pos_x > 210:
            car.pos_x -= car.vitesse_x
        if keys[pg.K_RIGHT] and car.pos_x < (580 - car.largeur_auto):
            car.pos_x += car.vitesse_x
        if keys[pg.K_DOWN] and car.pos_y < 600:
            car.pos_y += (car.vitesse_y + 5.5)
        elif keys[pg.K_UP] and car.pos_y > 200:
            car.pos_y -= car.vitesse_y
        # créé l'accélération du début
        rel_vit += vitesse_route * acceleration
        if acceleration < 1:
            acceleration += 0.002
        else:
            acceleration = 1
            pleine_vitesse = True
        if rel_vit > hauteur_window:
            rel_vit = 0
        afficher_route(rel_vit) #affiche la route
        boite_car = car.afficher() #affiche l'auto du joueur
        if pleine_vitesse == True: #une fois à pleine vitesse, le trafic apparait
            car_trafic1.afficher(boite_car, 500, car_trafic1.vitesse_random(), car_trafic1.auto_random(), points)
            car_trafic2.afficher(boite_car, 414, car_trafic2.vitesse_random(), car_trafic2.auto_random(), points)
            car_trafic3.afficher(boite_car, 328, car_trafic3.vitesse_random(), car_trafic3.auto_random(), points)
            car_trafic4.afficher(boite_car, 245, car_trafic4.vitesse_random(), car_trafic4.auto_random(), points)
            points += 1
        compteur(points, pointage)
        pg.display.update()
        pg.display.set_caption(str(int(clock.get_fps())))
        clock.tick(60)

#==================================================================================================#

menu() #Commence le progamme en amenant au menu principal
